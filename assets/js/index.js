const getCardData = src => {
    if (src === 'all') {
        const sources = [];
        nav.querySelectorAll('li span:not([data-src="all"])').forEach(el => sources.push(el.getAttribute('data-src')));

        return Promise.all(sources.map(source => fetch(`assets/flash_cards/${source}.json`).then(response => response.json())))
    } else {
        return fetch(`assets/flash_cards/${src}.json`).then(response => response.json());
    }
}

const getCard = async src => {
    let data = await getCardData(src);

    if (Array.isArray(data[0]) && Array.isArray(data[1])) {
        data = data.reduce((newArray, d) => [...newArray, ...d], []);
    }

    return data[Math.floor(Math.random() * data.length)]
}

const setCard = async data => {
    let showSpanish = Math.floor(Math.random() * 2) === 0;
    const { spanish, english, spanish_alt: spanishAlt, english_alt: englishAlt, force } = await data;
    const fullEnglish = englishAlt ? `${english} /<br> ${englishAlt}` : english;
    const fullSpanish = spanishAlt ? `${spanish} /<br> ${spanishAlt}` : spanish;

    if (force) {
        showSpanish = force === 'spanish';
    }

    cardFront.innerHTML = showSpanish ? fullSpanish : fullEnglish;
    cardBack.innerHTML = showSpanish ? fullEnglish : fullSpanish;
}

const addToTotalCounter = () => {
    if (testCheck.checked) {
        const matches = counter.textContent.match(/^Total: (\d+)\/(\d+)$/);

        counter.textContent = `Total: ${matches[1]}/${parseInt(matches[2], 10) + 1}`;
    }
}

const addToSuccessCounter = () => {
    if (testCheck.checked) {
        const matches = counter.textContent.match(/^Total: (\d+)\/(\d+)$/);

        counter.textContent = `Total: ${parseInt(matches[1], 10) + 1}/${matches[2]}`;
    }
}

const selectNavItem = (e) => {
    const item = e.target;
    const cardSrc = item.getAttribute('data-src');

    if (!item.classList.contains('selected')) {
        nav.querySelectorAll('li.selected').forEach(selectedItem => selectedItem.classList.remove('selected'));
        item.parentElement.classList.add('selected');
    }

    setCard(getCard(cardSrc));
    document.querySelector('.next').classList.toggle('hidden');
}

const toggleTesting = () => {
    counter.classList.toggle('hidden');
    answer.classList.toggle('hidden');
    counter.textContent = 'Total: 0/0';
}

const toggleCard = () => {
    card.classList.toggle('flip');
}

const nextCard = () => {
    const cardSrc = nav.querySelector('li.selected span').getAttribute('data-src');

    if (card.classList.contains('flip')) {
        card.dispatchEvent(new Event('click'));
        setTimeout(() => {
            addToTotalCounter();
            setCard(getCard(cardSrc));
        }, 500);
    } else {
        addToTotalCounter();
        setCard(getCard(cardSrc));
    }
}

const checkAnswer = (e) => {
    if (e.key === 'Enter') {
        const val = answer.value;
        const cardSrc = nav.querySelector('li.selected span').getAttribute('data-src');
        const cardValues = cardBack.textContent.split(' / ').map(s => s.toLowerCase());

        card.dispatchEvent(new Event('click'));

        setTimeout(() => {
            answer.value = '';
            card.dispatchEvent(new Event('click'));
            setTimeout(() => {
                addToTotalCounter();
                setCard(getCard(cardSrc));

                if (cardValues.includes(val.toLowerCase())) {
                    addToSuccessCounter();
                }
            }, 500);
        }, 1000);
    }
}

const nav = document.querySelector('nav ul');
const testCheck = document.querySelector('input[name="test"]');
const card = document.querySelector('.flip-card');
const cardFront = card.querySelector('.flip-card-front');
const cardBack = card.querySelector('.flip-card-back');
const nextBtn = document.querySelector('.next');
const counter = document.querySelector('.counter');
const answer = document.querySelector('.answer');

nav.querySelectorAll('li').forEach(item => item.addEventListener('click', selectNavItem));

testCheck.checked = testCheck.defaultChecked;
answer.value = '';

testCheck.addEventListener('change', toggleTesting);
card.addEventListener('click', toggleCard);
nextBtn.addEventListener('click', nextCard);
answer.addEventListener('keyup', checkAnswer);